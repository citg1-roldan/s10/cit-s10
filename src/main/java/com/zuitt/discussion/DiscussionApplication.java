package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

//	Retrieve all user
	@GetMapping("/users")
	public String getUsers(){
		return "All users retrieved.";
	}

//	Creating a new user
	@PostMapping("/users")
	public String createUser(){
		return "New user created.";
	}

//	Retrieving a single user

	@GetMapping("/users/{userid}")
	public String getUser(@PathVariable Long userid){
		return "Viewing details of user " + userid;
	}
//	Deleting a user
	@DeleteMapping("/users/{userid}")
	public String deleteUser(@PathVariable long userid, @RequestHeader(value = "Authorization")String logged){
	if(logged.isEmpty()){
		return "Unauthorized access";
	}
	else{
		return "The user "+userid+" has been deleted";
	}
}
//	Updating a user
	@PutMapping("/users/{userid}")
	@ResponseBody
	public User updateUser(@PathVariable Long userid, @RequestBody User user){
		return user;
	}


}
